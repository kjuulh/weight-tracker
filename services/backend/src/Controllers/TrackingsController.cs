using System;
using System.Collections.Generic;
using backend.Models;
using backend.Services;
using Microsoft.AspNetCore.Mvc;

namespace backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TrackingsController : ControllerBase
    {
        private readonly TrackingService _trackingService;

        public TrackingsController(TrackingService trackingService)
        {
            _trackingService = trackingService;
        }

        [HttpGet]
        public ActionResult<List<Tracking>> Get() => _trackingService.Get();

        [HttpGet("{id:length(24)}", Name = "GetTracking")]
        public ActionResult<Tracking> Get(string id)
        {
            var tracking = _trackingService.Get(id);

            if (tracking == null)
                return NotFound();
            return tracking;
        }

        [HttpPost]
        public ActionResult<Tracking> Create(Tracking tracking)
        {
            var tmpDate = tracking.Date.ToUniversalTime();
            tracking.Date = new DateTime(tmpDate.Year, tmpDate.Month, tmpDate.Day);
            _trackingService.Create(tracking);

            return CreatedAtRoute("GetTracking", new {id = tracking.Id.ToString()}, tracking);
        }

        [HttpPut("{id:length(24)}")]
        public ActionResult Update(string id, Tracking trackingIn)
        {
            var tracking = _trackingService.Get(id);

            if (tracking == null)
                return NotFound();
            
            _trackingService.Update(id, tracking);

            return NoContent();
        }

        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            var tracking = _trackingService.Get(id);

            if (tracking == null)
                return NotFound();
            
            _trackingService.Remove(tracking.Id);

            return NoContent();
        }
    }    
}