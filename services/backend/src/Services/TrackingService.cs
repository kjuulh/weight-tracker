using System;
using System.Collections.Generic;
using backend.Models;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;

namespace backend.Services
{
    public class TrackingService
    {
        private readonly IMongoCollection<Tracking> _trackings;

        public TrackingService(IConfiguration configuration)
        {
            var client = new MongoClient(configuration.GetConnectionString("WeightTrackingDb"));
            var database = client.GetDatabase("WeightTrackingDb");
            _trackings = database.GetCollection<Tracking>("Trackings");

            var isMongoLive = database.RunCommandAsync((Command<BsonDocument>) "{ping:1}").Wait(1000);
            if (isMongoLive)
            {
                Console.WriteLine("Is Connected");
            }
            else
            {
                throw new Exception("Couldn't Connect to MongoServer");
            }
            
            
        }

        public List<Tracking> Get() => _trackings.Find(tracking => true).ToList();
        
        public Tracking Get(string id) => _trackings.Find<Tracking>(tracking => tracking.Id == id).FirstOrDefault();

        public Tracking Create(Tracking tracking)
        {
            _trackings.InsertOne(tracking);
            return tracking;
        }

        public void Update(string id, Tracking trackingIn)
        {
            _trackings.ReplaceOne(tracking => tracking.Id == id, trackingIn);
        }

        public void Remove(Tracking trackingIn)
        {
            _trackings.DeleteOne(tracking => tracking.Id == trackingIn.Id);
        }

        public void Remove(string id)
        {
            _trackings.DeleteOne(tracking => tracking.Id == id);
        }
        
    }
}