
export interface Tracking {
  id: string,
  weight: number,
  date: Date,
}
