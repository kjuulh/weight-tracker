import {Component, Input, OnInit} from '@angular/core';
import {DatePipe} from "@angular/common"
import {Tracking} from "../../models/tracking";

@Component({
  selector: 'app-tracking',
  templateUrl: './tracking.component.html',
  styleUrls: ['./tracking.component.sass']
})
export class TrackingComponent implements OnInit {

  @Input() tracking: Tracking;

  constructor() { }

  ngOnInit() {
  }

}
