import { Component, OnInit } from '@angular/core';
import {TrackingsService} from "../../services/trackings/trackings.service";

@Component({
  selector: 'app-delete-all-trackings',
  templateUrl: './delete-all-trackings.component.html',
  styleUrls: ['./delete-all-trackings.component.sass']
})
export class DeleteAllTrackingsComponent implements OnInit {

  constructor(private ts: TrackingsService) { }

  ngOnInit() {
  }

  deleteAllTrackings(){
    this.ts.deleteAll();
  }
}
