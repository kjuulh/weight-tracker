import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteAllTrackingsComponent } from './delete-all-trackings.component';

describe('DeleteAllTrackingsComponent', () => {
  let component: DeleteAllTrackingsComponent;
  let fixture: ComponentFixture<DeleteAllTrackingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteAllTrackingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteAllTrackingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
