import { Component, OnInit } from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {Validators} from "@angular/forms";
import {TrackingsService} from "../../services/trackings/trackings.service";

@Component({
  selector: 'app-add-tracking',
  templateUrl: './add-tracking.component.html',
  styleUrls: ['./add-tracking.component.sass']
})
export class AddTrackingComponent implements OnInit {

  trackingForm = this.fb.group({
    weight: ["", Validators.required],
    date: ["", Validators.required]
  });

  response: string;

  constructor(private fb: FormBuilder,
              private ts: TrackingsService)
  { }

  ngOnInit() {
  }


  onSubmit() {
    this.ts.createTracking(this.trackingForm.value);
  }
}
