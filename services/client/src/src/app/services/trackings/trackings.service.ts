import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {Tracking} from "../../models/tracking";

@Injectable({
  providedIn: 'root'
})
export class TrackingsService {

  trackings$: Observable<Tracking[]>;
  url: string;
  endpoint: string;

  constructor(private http: HttpClient) {
    this.url = "http://localhost:8000";
    this.endpoint = "/api/Trackings";
  }

  getTrackings() {
    return this.http
      .get<Tracking[]>(this.url + this.endpoint);
  }

  getTracking(id: string){
    return this.http
      .get<Tracking>(this.url + this.endpoint + "/" + id);
  }

  createTracking(tracking){
    return this.http.post(this.url + this.endpoint, tracking).subscribe(resp => this.getTrackings());
  }

  deleteAll(){
    this.getTrackings().subscribe(data => data.forEach(d => this.delete(d.id)));
  }

  delete(id: string){
    this.http.delete(this.url + this.endpoint + "/" + id).subscribe(resp => console.log(resp));
  }
}
