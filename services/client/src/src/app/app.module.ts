import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import {HttpClientModule} from "@angular/common/http";
import { TrackingComponent } from './components/tracking/tracking.component';
import { AddTrackingComponent } from './components/add-tracking/add-tracking.component';
import {ReactiveFormsModule} from "@angular/forms";
import { DeleteAllTrackingsComponent } from './components/delete-all-trackings/delete-all-trackings.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TrackingComponent,
    AddTrackingComponent,
    DeleteAllTrackingsComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
