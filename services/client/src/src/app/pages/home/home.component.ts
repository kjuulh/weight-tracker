import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {TrackingsService} from "../../services/trackings/trackings.service";
import {Tracking} from "../../models/tracking";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {

  trackings$: Observable<Tracking[]>;

  constructor(private ts: TrackingsService) { }

  ngOnInit() {
  }

  refresh(){
    this.trackings$ = this.ts.getTrackings();
  }

}
